from setuptools import setup, find_packages
from os import path


this_directory = path.abspath(path.dirname(__file__))
with open(path.join(this_directory, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='mican',
    version='1.0.0',
    license='Proprietary',
    classifiers=[
        'License :: Other/Proprietary License'
    ],
    long_description=long_description,
    long_description_content_type='text/markdown',
    python_requires='>=3.4, <4',
    packages=find_packages(),
    install_requires=[
        'python-can',
    ]
    description='The mini CAN package extension for personal use. The main use is for kvaser.',
    author='Brent Barbachem',
    author_email='barbacbd@dukes.jmu.edu',
    include_package_data=True,
    dependency_links=[
        'https://pypi.org/simple/'
    ],
    zip_safe=False
)

