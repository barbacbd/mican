"""
Author: Brent Barbachem
Date:   5/8/20
"""
from enum import EnumMeta


def find(s, v):
    """
    Attempt to find the value in an enumeration. If the value v exists in the
    enumeration s, then return the value, otherwise return None.

    Example:
        from enum import Enum

        class TestExample(Enum):
            # do some enumeration stuff here
            A_EXAMPLE = 1
            B_EXAMPLE = 3
            C_EXAMPLE = 23423

        val = 3
        print(find(TestExample, val)))

    Prints:
        TestExample.B_EXAMPLE
    """
    if isinstance(s, EnumMeta):
        try:
            return [x for x in s if x.value == v][0]
        except IndexError:
            pass

    return None
