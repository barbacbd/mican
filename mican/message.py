"""
Author: Brent Barbachem
Date:   5/8/20
"""
from abc import ABCMeta, abstractmethod
from time import time


class miMessage:

    """
    Base class for an miCAN message.
    """

    def __init__(self,
                 pgn: int,
                 extended: bool = True,
                 rate: int = 1000
                 ):
        """
        Base class for all CAN Messages that will be used inside of
        systems and miCanInterfaces.

        :param pgn: Name modeled from the PGN of J1939 Messages but the user can
                    also think of this as the arbitration id used for the CAN messages
        :param extended: is the id for this message extended or not
        :param rate: rate in milliseconds. For instance if the user wants the rate
                     of the message to be 1 second the value of 1000 should be sent
                     as there are 1000 ms in 1 second
        """

        # time of the last update in milliseconds
        self.last_update = 0
        self._pgn = pgn
        self._rate_ms = 1.0 if rate <= 0 else rate / 1000.0
        self._extended_id = extended

    def __str__(self):
        """
        String representation of this message
        """
        return str(self._pgn)

    def __repr__(self):
        """
        Unique ID for the message
        """
        return str(self._pgn)

    def due_for_update(self):
        """
        Determine if this message is due for an update by finding the
        difference between the current time and the time of last update.
        If this time difference is greater than the rate then its time to update.
        """
        return time() - self.last_update > self.rate_ms

    @property
    def pgn(self):
        """
        Each instance is required to contain a PGN for that particular type of
        message. This base class is tied to a specific PGN. A PGN cannot/should not
        be reset once it has been initialized
        """
        return self._pgn

    @property
    def extended_id(self):
        """
        Return whether this message contains an extended id for the id field or not
        """
        return self._extended_id

    @property
    def rate_ms(self):
        """
        Each message will have rate associated with it. This is the update rate for
        the message. since the value is sent in whole milliseconds it will be return
        here as a fraction of seconds.
        """
        return self._rate_ms

    @abstractmethod
    def from_bytes(self, b: bytearray):
        """
        The function is only for the children to implement. The child class should
        fill in its variables based on the data stored in the bytearray (b).
        :param b: bytesarray from the CAN message data
        """
        raise NotImplementedError("Message base class does not convert from bytes.")

    @abstractmethod
    def to_bytes(self):
        """
        The function is only for the children to implement. The message will be converted
        to a byte array.
        """
        raise NotImplementedError("Message base class does not convert from bytes.")

    __metaclass__ = ABCMeta
