"""
Author: Brent Barbachem
Date:   5/8/20
"""

from can import Message
from sis.can.dlc.dlc import fill_to_proper_dlc, DLC


class miSystem:
    """
    Unknown System
    """

    def __init__(self, **kwargs):
        """
        Base class for all systems. Overwrite this class and add the new (custom)
        system to the project interface.

        Args:
            receiving:  List of Messages that will be received by this system
            sending:    list of Message that will be sent from this system
            fd:         Boolean, whether the message can support more than 8 bytes
            debug:      True provides more debugging information to the user
        """

        receiving = kwargs.get("receiving", [])
        sending = kwargs.get("sending", [])
        self.debug = kwargs.get("debug", False)

        # Dictionary that should map the PGN to the message type for
        # receiving messages ONLY.
        if isinstance(receiving, list):
            self.receiving = {x.pgn: x for x in receiving}
        else:
            raise TypeError("Expects list for receiving messages.")

        # the minimum update rate for all messages received in the system
        self.receiving_update_rate = min([v.rate_ms for k, v in self.receiving.items()]) if receiving else None

        # Dictionary that should map the PGN to the message type fpr
        # transmitted messages ONLY
        if isinstance(sending, list):
            self.sending = {x.pgn: x for x in sending}
        else:
            raise TypeError("Expects list for sending messages.")

        # the minimum update rate for all messages transmitted by the system
        self.sending_update_rate = min([v.rate_ms for k, v in self.sending.items()]) if sending else None

        def use_can_fd(msgs):
            """
            Does the set of messages contain any message requiring more than 8 bytes
            """
            for msg in msgs:
                data = msg.to_bytes()
                if data:
                    fill_to_proper_dlc(data)
                    if len(data) > int(DLC.DLC_8.value):
                        return True

            return False

        # attempt to find out if this system contains any messages that may
        # provide or receive more than 8 bytes
        self._can_fd = use_can_fd(self.receiving.values())
        if not self._can_fd:
            self._can_fd = use_can_fd(self.sending.values())

    @property
    def filters(self):
        """
        :return: Can filters that can/should be applied for this system
        """
        return [{
            "can_id": k,
            "can_mask": 0xFFFF if not v.extended_id else 0xFFFFFFFF,
            "extended": v.extended_id
        } for k, v in self.receiving.items()]

    def __str__(self):
        """
        String representation of the class
        """
        if not self.debug:
            return self.__doc__ if self.__doc__ else System.__doc__
        else:
            return "{}\n\tR - {}\n\tT - {}".format(
                self.__doc__ if self.__doc__ else System.__doc__,
                ', '.join([str(x) for x in self.receiving]),
                ', '.join([str(x) for x in self.sending])
            )

    def __contains__(self, item):
        """
        Return whether or not the item is one of the keys in the messages that this system
        intends to receive
        """
        return item in self.receiving

    def __call__(self, *args, **kwargs):
        """
        Update the message in this system based on the following

        Args:
            arbitration_id: ID or PGN of the message that should be updated
            data: bytearray of can data to update the message with
        """
        arbitration_id = kwargs.get("arbitration_id", None)
        data = kwargs.get("data", None)

        if arbitration_id and data and arbitration_id in self:
            self.receiving[arbitration_id].from_bytes(data)

    @property
    def can_fd(self):
        """
        :return: True if any message in this system is contains more bytes
                 than the normal 8 bytes
        """
        return self._can_fd

    @property
    def updates(self):
        """
        Determine which messages require an update.
        :return: Return the list of can messages from the messages that require an update
        """

        messages = []

        for k, v in self.sending.items():
            if v.due_for_update():
                data = v.to_bytes()
                fill_to_proper_dlc(data)

                messages.append(Message(
                    arbitration_id=k,
                    data=data,
                    is_error_frame=False,
                    is_extended_id=v.extended_id,
                    dlc=len(data),
                    is_fd=len(data) > int(DLC.DLC_8.value)
                ))

        return messages
