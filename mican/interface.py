"""
Author: Brent Barbachem
Date:   5/7/20
"""
from threading import Lock, Thread
from mican.system import miSystem
from can import Bus, Message, CanError, BufferedReader, Notifier
from time import sleep


class miInterface:

    def __init__(self, **kwargs):
        """
        The class is the base interface for all of miCAN bus. The class contains
        a reference to each system on the bus. All messages will arrive at this interface
        or transmit from this interface, however each system is tasked with managing the
        data stored in each of its messages.

        Args:
            bitrate:       bitrate for the can bus. All systems MUST use the same bitrate for communication to occur.
                           If no bitrate is provided the default value is 125 kpbs.
            channel:       channel on the can bus to use. If no channel is provided, 0 will be used.
            timeout:       Number of seconds to use as the timeout for sending and receiving messages.
                           If no timeout is provided 0.1 seconds will be used.
            bustype:       Type of can bus to use for this interface (ex: 'kvaser'). If no bustype is provided,
                           kvaser will be used.
            systems:       List of Systems that makes up this can interface (default to an empty list).
            data_biterate: The bit rate to use when fd is set to true. This bit rate is faster than the normal
                           bitrate
            debug:         should this interface run in debug mode or not (default to False).
        """
        bitrate = kwargs.get("bitrate", 125000)
        channel = kwargs.get("channel", 0)
        bustype = kwargs.get("bustype", 'kvaser')
        data_bitrate = kwargs.get("data_bitrate", 500000)
        self._data_bitrate = data_bitrate

        timeout = kwargs.get("timeout", 0.01)
        # limit th timeout to no more than 10 seconds - default to 10 milliseconds
        # since the python-can timeout is in milliseconds
        self._timeout = timeout if isinstance(timeout, float) and 0.0 < timeout < 10.0 else 0.01

        self.debug = kwargs.get("debug", False)

        # Each system that is provided should also know if the CAN FD is set.
        # in the event that CAN FD is set, then the system needs to set that
        # for each of its messages
        systems = kwargs.get("systems", [])
        if isinstance(systems, list):
            self._systems = [x for x in systems if isinstance(x, miSystem)]
        else:
            raise TypeError("Expects a list of Systems.")
        [print("Loading: {}".format(str(x))) for x in self._systems]


        # Should this interface set fd (can_fd) based on the systems below it
        self._can_fd = True in [x.can_fd for x in self._systems]

        self.bus = Bus(
            channel=channel,
            bitrate=bitrate,
            bustype=bustype,
            fd=self._can_fd,
            data_bitrate=self._data_bitrate
        )

        self.lock = Lock()  # lock to keep data synchronous
        self._threads = []  # link to the read and write threads
        self._running = False  # status of the interface

        self._rx_thread_sleep = None
        _rx = [x.receiving_update_rate for x in self._systems if x.receiving_update_rate]
        if _rx:
            self._rx_thread_sleep = min(_rx)

        self._tx_thread_sleep = None
        _tx = [x.sending_update_rate for x in self._systems if x.sending_update_rate]
        if _tx:
            self._tx_thread_sleep = min(_tx)

        # The buffered reader and notifier allow the interface to buffer messages
        # and receive alerts when new messages have arrived
        self._can_buffer = BufferedReader()
        self._can_notifier = Notifier(self.bus, [self._can_buffer])

        # grab all of the filters for all of the systems that are a part of this interface
        filters = []
        for x in self._systems:
            filters.extend(x.filters)
        self.bus.set_filters(filters)

    @property
    def can_fd(self):
        """
        Will this interface allow messages larger than 8 bytes (up to 64 bytes)
        """
        return self._can_fd

    @property
    def data_bitrate(self):
        """
        Only applicable when can_fd is true
        """
        return self._data_bitrate if self.can_fd else 0

    @property
    def running(self):
        return self._running

    def __call__(self, *args, **kwargs):
        """
        When the user invokes the () function on the object, the system will start
        by creating two threads (reader and writer). If the () is called while the
        interface is running, all threads will be shtudown
        """
        if self._running:
            self.shutdown()
        else:
            self._threads.clear()

            if self._rx_thread_sleep:
                self._threads.append(Thread(target=self._rx_thread, args=()))

            if self._tx_thread_sleep:
                self._threads.append(Thread(target=self._tx_thread, args=()))

            self._running = True
            [x.start() for x in self._threads]

    def _rx_thread(self):
        """
        Read thread for this interface
        """
        while self._running:
            self.read()

            sleep(self._rx_thread_sleep)

    def _tx_thread(self):
        """
        Write thread for this interface
        """
        while self._running:
            self.write()

            sleep(self._tx_thread_sleep)

    def shutdown(self):
        """
        Stop this interface by joining any threads
        """
        self._running = False

        for thread in self._threads:
            if isinstance(thread, Thread):
                thread.join()

        self._threads.clear()

    def _ignore_error(self, msg):
        """
        :return: True when the error can be ignored. This is the case when it is not an error frame
                 or when it is an error frame, we are not expecting more than 8 bytes but the message
                 contains more than 8 bytes of data.
        """
        return not msg.is_error_frame or (msg.is_error_frame and not self._can_fd and msg.dlc > 8)

    def read(self):
        """
        Attempt to read data from the can bus and update the message in the
        system that contains the message. To prevent too many messages from being
        queued (blocking our FIFO system) we will read half of the messages in the
        buffer unless there is only one message.
        """
        for x in range(0, max(int(.5 * self._can_buffer.buffer.qsize()), 1)):

            # future use, use the timeout here
            msg = self._can_buffer.get_message(timeout=0)
            if self.debug and msg is not None:
                print("Recieved Message: " + str(hex(msg.arbitration_id)) + ", " + str(msg.data))            

            if msg is not None and self._ignore_error(msg) and msg.data is not None:
                with self.lock:
                    [x(
                        arbitration_id=msg.arbitration_id,
                        data=msg.data
                    ) for x in self._systems if msg.arbitration_id in x]

    def write(self):
        """
        Make a list of all messages from all systems that need to be updated.
        Send these messages on the CAN bus with a time delay between messages
        to ensure that the data is written to the bus
        """
        messages_to_transmit = []

        with self.lock:
            for x in self._systems:
                messages_to_transmit.extend(x.updates)

        for can_msg in messages_to_transmit:
            if self.debug:
                print("Sending Message: " + str(hex(can_msg.arbitration_id)) + ", " + str(can_msg.data))
            self.send(can_msg)
            sleep(0.05)

    def send(self, msg: Message) -> None:
        """
        Attempt to send the data via the CAN Bus
        :param msg: CAN Message to send through the bus
        """
        try:
            """ 
            Don't you hate when the user before you didn't Flush? 
            Flush here before sending, just in case!
            """
            self.bus.flush_tx_buffer()

            # Future use: use the timeout here
            self.bus.send(msg, 0.0)
        except CanError:
            print("ERROR: failed to send: {}".format(msg))
