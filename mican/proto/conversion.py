"""
Author: Brent Barbachem
Date:   5/8/20
"""
from google.protobuf.json_format import ParseDict, ParseError, MessageToDict
from google.protobuf.message import Message


def fill_protobuf(msg, **kwargs):
    """
    The user can pass the msg (any protobuf message) and any additional arguments
    and fill in the fields of the protobuf message with these fields. Any fields
    that are not recognized or their types are incorrect will be ignored.

    In the event of Nested protobuf messages the user `SHOULD` run the function
    multiple times as only one nested layer can be filled out as a limitation.

    Ex:

        ExampleMessage {
            optional uint32 a = 1;
            optional uint32 b = 2;
        }

        ExampleMessage2 {
            optional ExampleMessage em = 1;
            optional uint32 test = 2;
        }

        test1 = ExampleMessage()
        fill_protobuf(test1, a=15, b=16)

        test2 = ExampleMessage2()
        fill_protobuf(test2, em=test1, test=20)

        print(test2)

    Result:
        em {
            a: 15
            b: 16
        }
        test: 20

    :param msg: a protobuf message to attempt to fill in
    :param kwargs: dictionary containing all of the datafields and their values that
    should be filled in
    """

    try:
        ParseDict({
            k: MessageToDict(v) if isinstance(v, Message) else v
            for k, v in kwargs.items()
        }, msg, ignore_unknown_fields=True)
    except (ParseError, TypeError) as e:
        pass
